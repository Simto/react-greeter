import React from 'react';
import ReactDOM from 'react-dom';
import App from './App'

import greeterForm from './components/greeter-form';
import logoFile from './images/logo.png';

// Uygulama stilleri
import './styles/main.scss';

const appContainer = document.getElementById('app');
ReactDOM.render(<App />, appContainer);