import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import logo from '../images/logo.png';

class Logo extends Component {
	render() {
		return (
			<img src={logo} />
		);
	}
}

export default Logo;