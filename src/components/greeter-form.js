import React, { Component } from 'react';

class GreeterForm extends Component {
	constructor(props) {
		super(props)
		this.state = {name: ''};

		this.greeter = this.greeter.bind(this);
		this.nameChange = this.nameChange.bind(this);
	}

	nameChange(event) {
		this.setState({name: event.target.value});
	}

	greeter(event) {
		console.log(this.state.name);
		event.preventDefault();
	}

	render() {
		return(
			<React.Fragment>
				<form onSubmit={this.greeter}>
					<input type="text" placeholder="Type your name"  value={this.state.value} onChange={this.nameChange} />
					<button type="submit">Say hello!</button>
				</form>
				<div className="message" />
			</React.Fragment>
		)
	}
}
export default GreeterForm;