import React, { Component } from 'react';
import GreeterForm from "./components/greeter-form";
import Logo from "./components/logo";

class App extends Component {
	render() {
		return (
            <React.Fragment>
                <Logo />
                <GreeterForm />
            </React.Fragment>
		);
	}
}

export default App;