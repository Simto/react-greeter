const HtmlWebpackPlugin = require('html-webpack-plugin');
const fs = require('fs');
const path = require('path');

__dirname == process.cwd() == 'webpack-demo'

/*************************/
// Node'un fs apisi ile src içindeki tüm *.html dosyalarını export etmeyi dene.
/***************************/

// Javascript dosyalarını babel ile transpile et.
const jsRule = {
	test: /\.js$/,
	exclude: /node_modules/,
	loader: 'babel-loader'
};

// Png gif ve jpg dosyalarını base64 olarak embed et
// ya da 200Kb den büyükse export et.
const imageRule = {
	test: /\.(png|gif|jpg)$/,
	use: [
		{
			loader: 'url-loader',
			options: {
                outputPath: 'images/',
                name: '[hash:10].[ext]',
                limit: 200000,
            }
		}
	]
};

// Css dosyaları
const styleRule = {
	test: /\.scss$/,
	exclude: /node_modules/,
	use: ['style-loader', 'css-loader', 'sass-loader', 'postcss-loader']
};

// Font dosyaları
const fontRule = {
	test: /\.(woff|ttf)$/,
	use: {
		loader: 'file-loader',
		options: {
			outputPath: 'fonts/'
		}
	}
};

function generateHtmlPlugins (srcDir) {
	const htmlFiles = fs.readdirSync(path.resolve(__dirname, srcDir))
	return htmlFiles
	.filter(item => {
		const parts = item.split('.')
		const extension = parts[parts.length-1]
		return extension == "html"
	})
	.map(item => {
		return new HtmlWebpackPlugin({
			filename: `./${item}`,
			template: path.resolve(__dirname, `${srcDir}${item}`)
		})
	})
};

const htmlPlugins = generateHtmlPlugins('./src/');

const config = {
	mode: 'development',
	module: {
		rules: [
			jsRule,
			imageRule,
			styleRule,
			fontRule
		]
	},
	plugins: htmlPlugins
};

module.exports = config;